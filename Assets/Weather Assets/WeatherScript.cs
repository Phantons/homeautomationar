﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;
using System;
using System.Globalization;
using TMPro;
using System.Text.RegularExpressions;

public class WeatherScript : MonoBehaviour {

    private const string URL = "https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/diaria/";
    // Madrid
    private const string location = "28079";

    private const string API_KEY = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtYXJxdWl0b3MubWNtQGdtYWlsLmNvbSIs" +
        "Imp0aSI6IjNkZDA0Y2Q4LTk4MmYtNGU5ZS04NTU1LTIwZGI2OTIyYjVkZiIsImlzcyI6IkFFTUVUIiwiaWF" +
        "0IjoxNTE5OTIyNDg3LCJ1c2VySWQiOiIzZGQwNGNkOC05ODJmLTRlOWUtODU1NS0yMGRiNjkyMmI1ZGYiLCJy" +
        "b2xlIjoiIn0.OJQ7Y9ywbaFGkYlbk9LEv8xllS_A0buf3-TXDIALpJI";

    private const int NUMBER_OF_SAMPLES = 4;
    private int[] chanceOfPrecipitationPerDay = new int[NUMBER_OF_SAMPLES];
    private int[] maxTempPerDay = new int[NUMBER_OF_SAMPLES];
    private int[] minTempPerDay = new int[NUMBER_OF_SAMPLES];
    private int[] skyState = new int[NUMBER_OF_SAMPLES];

    //public TextMeshPro todayTemperatureObject;
    //public TextMeshPro tomorrowTemperatureObject;

    public SynchronizedTextBehaviour todayTempController;
    public SynchronizedTextBehaviour tomorrowTempController;

    public GameObject todayImg;
    public GameObject tomorrowImg;

    public Transform cloudy;
    public Transform sunny;
    public Transform rainy;
    public Transform snowy;

    private const int MIN_SUNNY = 10;
    private const int MIN_CLOUDY = 50;

    private const int MIN_SNOWY = 4;

    void Start () {

        RestClient.Get(URL + location + "/?api_key=" + API_KEY).Then(res => {
            JSONObject urlOfData = new JSONObject(res.Text);

            // Received a valid data
            if (urlOfData["estado"].n == 200)
            {
                string finalURL = urlOfData["datos"].str;

                RestClient.Get(finalURL).Then(finalRes => {

                    JSONObject data = new JSONObject(finalRes.Text);

                    fetchData(data);

                    Debug.Log("Max temp today " + maxTempPerDay[0] + " min temp today " + minTempPerDay[0]);
                    Debug.Log("Max temp tomorrow " + maxTempPerDay[1] + " min temp tomorrow " + minTempPerDay[1]);

                    todayTempController.setText(maxTempPerDay[0] + "º/" + minTempPerDay[0] + "º");
                    tomorrowTempController.setText(maxTempPerDay[1] + "º/" + minTempPerDay[1] + "º");

                    Debug.Log("Chance precipitation today " + chanceOfPrecipitationPerDay[0]);
                    Debug.Log("Chance precipitation tomorrow " + chanceOfPrecipitationPerDay[1]);

                    Debug.Log("skyState today " + skyState[0]);
                    Debug.Log("skyState tomorrow " + skyState[1]);

                    showObject(0, todayImg.transform.position);
                    showObject(1, tomorrowImg.transform.position);
                });

            }

        });

	}

    private void showObject(int day, Vector3 position)
    {
        if (skyState[day] == 11)
        {
            Instantiate(sunny, position, Quaternion.identity);
        } else if (skyState[day] > 11 && skyState[day] <= 20)
        {
            Instantiate(cloudy, position, Quaternion.identity);
        } else
        {
            if (maxTempPerDay[day] < MIN_SNOWY)
            {
                Instantiate(snowy, position, Quaternion.identity);
            } else
            {
                Instantiate(rainy, position, Quaternion.identity);
            }
        }
    }

    private void fetchData(JSONObject data)
    {
        JSONObject day = data[0]["prediccion"]["dia"];

        JSONObject dayJSON = null;
        int dayIterator = 0;
        int sampleIterator = 0;
        while (dayIterator < day.list.Count)
        {
            dayJSON = day[dayIterator];

            string dateString = dayJSON["fecha"].str;
            DateTime date = DateTime.ParseExact(dateString, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            if (date.Day >= DateTime.Now.Day)
            {
                chanceOfPrecipitationPerDay[sampleIterator] = (int)dayJSON["probPrecipitacion"][0]["value"].n;
                maxTempPerDay[sampleIterator] = (int)dayJSON["temperatura"]["maxima"].n;
                minTempPerDay[sampleIterator] = (int)dayJSON["temperatura"]["minima"].n;

                int time = 0;
                string skyStateThisDay = "";

                while (time < dayJSON["estadoCielo"].list.Count) {
                    skyStateThisDay = dayJSON["estadoCielo"][time]["value"].str;

                    if (skyStateThisDay.Length > 0)
                        break;
                    time++;
                }

                if (skyStateThisDay.Length > 0)
                {
                    string pattern = "(\\d+)";
                    Match match = System.Text.RegularExpressions.Regex.Match(skyStateThisDay, pattern);
                    skyState[sampleIterator] = Int32.Parse(match.Groups[1].Value);
                }
                sampleIterator++;

                if (sampleIterator >= NUMBER_OF_SAMPLES)
                    break;
            }

            dayIterator++;
        }
    }
	
	void Update () {
		
	}
}
