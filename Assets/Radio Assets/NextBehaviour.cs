﻿using Lean.Touch;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;

public class NextBehaviour : MonoBehaviour
{
    public GameObject radioController;

    private RadioControllerBehaviour controller;
    private ThreadStart tStart;

    // Use this for initialization
    void Start()
    {
        controller = radioController.GetComponent<RadioControllerBehaviour>();

        tStart = new ThreadStart(sendMessage);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void onSelectSet(LeanFinger finger)
    {
        if ((controller.getLastClick() + controller.getClickThreshold()) < controller.now())
        {
            Thread t = new Thread(tStart);
            t.Start();

            controller.setLastClick(controller.now());
        }
    }

    private void sendMessage()
    {
        SendData.SendToServer(SendData.DataType.NEXT, 0, callback);
    }

    private int callback(SendData.DataType type, string message)
    {
        if (type.Equals(SendData.DataType.NEXT))
            SendData.SendToServer(SendData.DataType.CURRENTSONG, 0, callback);
        else
            controller.setSongText(message);

        return 0;
    }
}