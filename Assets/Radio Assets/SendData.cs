﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Net.Sockets;
using System.IO;
using Object = System.Object;
using System.Text;
using System.Text.RegularExpressions;

public class SendData : ScriptableObject
{
    private static TcpClient client;
    private static NetworkStream networkStream;
    private static StreamWriter clientStreamWriter;
    private static Object mutex = new Object();

    private const string SERVER_IP = "10.7.2.71";
    private const int PORT = 6600;

    public enum DataType
    {
        STATUS,
        PLAY,
        PAUSE,
        NEXT,
        PREVIOUS,
        CURRENTSONG,
        SETVOL
    };

    private const int STATUS_LENGTH = 14;

    public static void SendToServer(DataType dataType, int data, Func<DataType, string, int> callback)
    {
        string messageToSend = null;

        switch (dataType)
        {
            case DataType.STATUS:
                messageToSend = "status\n";
                break;
            case DataType.PLAY:
                messageToSend = "play\n";
                break;
            case DataType.PAUSE:
                messageToSend = "pause\n";
                break;
            case DataType.NEXT:
                messageToSend = "next\n";
                break;
            case DataType.PREVIOUS:
                messageToSend = "previous\n";
                break;
            case DataType.CURRENTSONG:
                messageToSend = "currentsong\n";
                break;
            case DataType.SETVOL:
                messageToSend = "setvol {" + data +"}\n";
                break;
        }

        lock (mutex)
        {
            client = new TcpClient();

            client.Connect(SERVER_IP, PORT);
            networkStream = client.GetStream();

            Debug.Log("Send " + messageToSend);

            SendByNetworkStream(networkStream, messageToSend);

            String dataReceived = "";

            StreamReader reader = new StreamReader(networkStream);
            dataReceived = reader.ReadLine();

            // MPD always send it's version.
            if (dataReceived.StartsWith("OK MPD"))
            {
                dataReceived = "";
                switch (dataType)
                {
                    case DataType.STATUS:
                        for (int i = 0; i < STATUS_LENGTH; i++)
                        {
                            dataReceived = String.Concat(dataReceived + "\n", reader.ReadLine());
                        }
                        break;
                    case DataType.CURRENTSONG:
                        dataReceived = reader.ReadLine();
                        string[] dataReceivedSplitted = Regex.Split(dataReceived, "/");
                        dataReceived = dataReceivedSplitted[dataReceivedSplitted.Length - 1];
                        break;
                    case DataType.PLAY:
                    case DataType.PAUSE:
                    case DataType.NEXT:
                    case DataType.PREVIOUS:
                        dataReceived = reader.ReadLine();
                        break;
                }
            }

            Debug.Log("Data received " + dataReceived);

            callback(dataType, dataReceived);

            client.Close();
        }
    }

    private static void SendByNetworkStream(NetworkStream stream, String message)
    {
        byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(message);
        networkStream.Write(bytesToSend, 0, bytesToSend.Length);
    }
}