﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using TMPro;
using UnityEngine;
using Object = System.Object;

public class RadioControllerBehaviour : MonoBehaviour {

    private Boolean isPlaying = false;
    private Boolean hasChanged = false;

    private Boolean hasVolumeChanged = false;
    private int volume = 0;

    public GameObject playObject;
    public GameObject pauseObject;
    public TextMeshPro songTextObject;

    private const long CLICK_THRESHOLD = 1000; // 1 sec
    private long last_click = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
    private Object mutex = new Object();

    private SynchronizedTextBehaviour songTextController;

    ThreadStart tStart;

    // Use this for initialization
    void Start()
    {
        pauseObject = GameObject.Find("s-pause");
        playObject = GameObject.Find("s-play");
        setIsPlaying(false);

        songTextController = songTextObject.GetComponent<SynchronizedTextBehaviour>();

        tStart = new ThreadStart(sendMessage);
        Thread t = new Thread(tStart);
        t.Start();
    }

    private void sendMessage()
    {
        SendData.SendToServer(SendData.DataType.STATUS, 0, callback);
    }

    // Update is called once per frame
    void Update()
    {
        if (hasChanged)
        {
            playObject.SetActive(!isPlaying);
            pauseObject.SetActive(isPlaying);
            hasChanged = false;
        }
    }

    public int callback(SendData.DataType type, string response)
    {
        Debug.Log("RadioController: " + response);

        if (type.Equals(SendData.DataType.STATUS))
        {
            string[] status = Regex.Split(response, "\n");

            foreach (string st in status)
            {
                if (st.Contains("state"))
                {
                    string[] state = Regex.Split(st, ": ");

                    if (state.Length != 2)
                    {
                        Debug.Log("Bad format? " + st);
                    } else
                    {
                        if (state[1].Equals("play"))
                        {
                            this.setIsPlaying(true);
                        }
                    }
                } else if (st.Contains("volume"))
                {
                    string[] volumeSplitted = Regex.Split(st, ": ");
                    if (volumeSplitted.Length != 2)
                    {
                        Debug.Log("Bad format? " + st);
                    }
                    else
                    {
                        int result = 0;
                        if (Int32.TryParse(volumeSplitted[1], out result))
                            setVolume(result);
                    }
                }
            }

            SendData.SendToServer(SendData.DataType.CURRENTSONG, 0, callback);
        } else if (type.Equals(SendData.DataType.CURRENTSONG))
        {
            this.setSongText(response);
        }

        return 0;
    }

    public void setIsPlaying(Boolean play)
    {
        lock (mutex) {
            isPlaying = play;
            hasChanged = true;
        }
    }

    public void setVolume(int _volume)
    {
        lock(mutex)
        {
            this.volume = _volume;
            hasVolumeChanged = true;
        }
    }

    public int getVolume()
    {
        int _volume = -1;
        lock (mutex)
        {
            _volume = this.volume;
            hasVolumeChanged = false;
        }

        return _volume;
    }

    public Boolean HasVolumeChanged()
    {
        Boolean _hasVolumeChanged = false;

        lock (mutex)
        {
            _hasVolumeChanged = this.hasVolumeChanged;
        }

        return _hasVolumeChanged;
    }

    public void setSongText(string songName)
    {
        if (songName.Equals("OK"))
            setIsPlaying(false);
        else
            songTextController.setText(songName);
    }

    public long getLastClick()
    {
        return last_click;
    }

    public void setLastClick(long _last_click)
    {
        last_click = _last_click;
    }

    public long now()
    {
        return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
    }

    public long getClickThreshold()
    {
        return CLICK_THRESHOLD;
    }
}
