﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SongTextBehaviour : MonoBehaviour {

    public TextMeshPro myTextMeshObject;

    private string myText = null;
    private object mutex = new object();

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        if (myText != null) {
            myTextMeshObject.text = myText;
            myText = null;
        }
    }

    public void setText(string text)
    {
        lock(mutex)
        {
            myText = text;
        }
    }
}
