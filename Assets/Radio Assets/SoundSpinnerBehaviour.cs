﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class SoundSpinnerBehaviour : MonoBehaviour {

    public GameObject radioController;
    public Slider mainSlider;

    private RadioControllerBehaviour controller;
    private ThreadStart tStart;

    // Use this for initialization
    void Start () {
        controller = radioController.GetComponent<RadioControllerBehaviour>();
        tStart = new ThreadStart(sendMessage);
    }
	
	// Update is called once per frame
	void Update () {
	    if (controller.HasVolumeChanged())
        {
            mainSlider.value = (float)(controller.getVolume() / 100);
        }
	}

    public void OnValueChange()
    {
        Thread t = new Thread(tStart);
        t.Start();
    }

    public void sendMessage()
    {
        controller.setVolume((int)(mainSlider.value * 100));
        SendData.SendToServer(SendData.DataType.SETVOL, (int)(mainSlider.value * 100), callback);
    }

    private int callback(SendData.DataType type, string message)
    {
        return 0;
    }
}
