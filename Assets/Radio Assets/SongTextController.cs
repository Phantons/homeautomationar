﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SongTextController : MonoBehaviour {

    public TextMeshPro myTextMeshObject;
    private TextMeshPro cloneMyTextMeshObject;

    private RectTransform textMeshRectTransform;

    const float SCROLL_SPEED = 200;

    void Awake()
    {
        textMeshRectTransform = myTextMeshObject.GetComponent<RectTransform>();

        cloneMyTextMeshObject = Instantiate(myTextMeshObject) as TextMeshPro;
        RectTransform cloneRectTransform = cloneMyTextMeshObject.GetComponent<RectTransform>();
        cloneRectTransform.SetParent(textMeshRectTransform);
        cloneRectTransform.anchorMin = new Vector2(1, 0.5f);
        cloneRectTransform.localScale = new Vector3(1, 1, 1);
    }
	
    IEnumerator Start()
    {
        float width = 0;
        Vector3 startPosition;
        float scrollPosition = 0;

        width = textMeshRectTransform.sizeDelta.x;
        startPosition = textMeshRectTransform.position;

        while (true)
        {
            if (myTextMeshObject.havePropertiesChanged)
            {
                width = textMeshRectTransform.sizeDelta.x;
                cloneMyTextMeshObject.text = myTextMeshObject.text;
            }

            scrollPosition += SCROLL_SPEED * Time.deltaTime;

            textMeshRectTransform.position = new Vector3(-(scrollPosition % width), startPosition.y, startPosition.z);

            yield return null;
        }
    }
}
